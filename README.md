# BMP280 Temperature, Pressure, & Altitude Sensor

[![Build Status](https://travis-ci.com/pimoroni/bmp280-python.svg?branch=master)](https://travis-ci.com/pimoroni/bmp280-python)
[![Coverage Status](https://coveralls.io/repos/github/pimoroni/bmp280-python/badge.svg?branch=master)](https://coveralls.io/github/pimoroni/bmp280-python?branch=master)
[![PyPi Package](https://img.shields.io/pypi/v/bmp280.svg)](https://pypi.python.org/pypi/bmp280)
[![Python Versions](https://img.shields.io/pypi/pyversions/bmp280.svg)](https://pypi.python.org/pypi/bmp280)

Suitable for measuring ambient temperature, barometric pressure, and altitude, the BMP280 is a basic weather sensor.

# Installing
Stable library from PyPi:

* `sudo pip install bmp280`

Rules for installing:

* `git clone https://gitlab.com/juniorf11/dev-bmp280.git`
* `cd dev-bmp280`
* `sudo ./install.sh`


latest dev script python jun :
tpa-bmp280.py 

Source library from GitHub:
* `git clone https://github.com/pimoroni/bmp280-python`


